// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('loginTab', {
    url: '/loginTab',
    abstract: true,
    cache: false,
    templateUrl: 'templates/loginTab.html',
    controller: 'LoginCtrl'
  
  })
  
    .state('loginTab.login', {
    url: '/login',
  cache : false,
    views: {
      'login': {
        templateUrl: 'templates/login.html'
      }
    }
  })

      .state('registerTab', {
    url: '/registerTab',
    abstract: true,
    cache: false,
    templateUrl: 'templates/registerTab.html',
    controller : 'RegisterCtrl'
  })
  
  .state('registerTab.register', {
    url: '/register',
  cache : false,
    views: {
      'register': {
        templateUrl: 'templates/register.html'
      }
    }
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

    .state('app.tutor', {
      url: '/tutor',
      views: {
        'menuContent': {
          templateUrl: 'templates/tutor.html',
          controller: 'TutorCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  })

    .state('app.history', {
      url: '/history',
      views: {
        'menuContent': {
          templateUrl: 'templates/history.html',
          controller: 'HistoryCtrl'
        }
      }
    })

    .state('app.course', {
      url: '/course',
      views: {
        'menuContent': {
          templateUrl: 'templates/course.html',
          controller: 'CourseCtrl'
        }
      }
    })
.state('app.ftmk', {
      url: '/ftmk',
      views: {
        'menuContent': {
          templateUrl: 'templates/FTMK.html',
          controller: 'FTMKCtrl'
        }
      }
    })
.state('app.fkekk', {
      url: '/fkekk',
      views: {
        'menuContent': {
          templateUrl: 'templates/FKEKK.html',
          controller: 'FKEKKCtrl'
        }
      }
    })
.state('app.ftk', {
      url: '/ftk',
      views: {
        'menuContent': {
          templateUrl: 'templates/FTK.html',
          controller: 'FTKCtrl'
        }
      }
    })
.state('app.fkm', {
      url: '/fkm',
      views: {
        'menuContent': {
          templateUrl: 'templates/FKM.html',
          controller: 'FKMCtrl'
        }
      }
    })
.state('app.fke', {
      url: '/fke',
      views: {
        'menuContent': {
          templateUrl: 'templates/FKE.html',
          controller: 'FKECtrl'
        }
      }
    })
.state('app.fkp', {
      url: '/fkp',
      views: {
        'menuContent': {
          templateUrl: 'templates/FKP.html',
          controller: 'FKPCtrl'
        }
      }
    })
.state('app.fptt', {
      url: '/fptt',
      views: {
        'menuContent': {
          templateUrl: 'templates/FPTT.html',
          controller: 'FPTTCtrl'
        }
      }
    })
    .state('app.reminder', {
      url: '/reminder',
      views: {
        'menuContent': {
          templateUrl: 'templates/reminder.html',
          controller: 'ReminderCtrl'
        }
      }
    })

    .state('app.reminderdetail', {
      url: '/reminderdetail',
      views: {
        'menuContent': {
          templateUrl: 'templates/reminderdetail.html',
          controller: 'ReminderdetailCtrl'
        }
      }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/loginTab/login');
})

