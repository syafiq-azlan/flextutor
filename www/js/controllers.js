angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $state, $location) {


$scope.username = localStorage.getItem("username");
$scope.matric = localStorage.getItem("matric");
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  $scope.doLogout = function()
  {
    $location.path('/loginTab/login');
  }

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})

.controller('TutorCtrl', function($scope, $http, $ionicPopup, $filter) {

    $scope.regTutor = {};

    $scope.doRegister = function () {

    var subject = $scope.regTutor.subject;
    var faculty = $scope.regTutor.faculty;
    var subjectOther = $scope.regTutor.subjectOther;
    var date = $scope.regTutor.date;
    var time = $scope.regTutor.time;
    var venue = $scope.regTutor.venue;
    var description = $scope.regTutor.description;
    var userId = 1;

//dd mm yyyy
 //Wed May 03 2017 00:00:00 GMT+0800 (Malay Peninsula Standard Time)

    var dateAsString = $filter('date')(date, "dd-MM-yyyy");
    var ntime = $filter('date')(time,'h:mma');


    //console.log("time : "+ ntime);

    $http.get("https://syafiqevo.000webhostapp.com/flex/regTutor.php?&subject="+subject+"&faculty="+faculty+"&date="+dateAsString+"&time="+ntime+"&venue="+venue+"&description="+description+"&userId="+userId).
    success(function(data) {
      console.log("berjaya");
                 $ionicPopup.alert({
           title: 'Success',
           template: 'You have registered a tutor session'
         });
    })
    .error(function(data) {
           $ionicPopup.alert({
           title: 'Error',
           template: 'Registration failed'
         });
    })
  }

})

.controller('HistoryCtrl', function($scope, $stateParams,$ionicPopup) {
  $scope.nama ="aaaaaa";
$scope.showAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'Subject',
       template: 'Time: <br /> Date: <br /> Venue: <br /> Description:'
       
     });
   }
})
.controller('RegisterCtrl', function($scope, $stateParams, $ionicPopup, $http, $location) {

$scope.regData = {}

$scope.doRegister = function()
{
  var username = $scope.regData.username;
  var password = $scope.regData.password;
  var matric = $scope.regData.matric;
  var faculty = $scope.regData.faculty;

      $http.get("https://syafiqevo.000webhostapp.com/flex/regUser.php?&username="+username+"&password="+password+"&matric="+matric+"&faculty="+faculty).
    success(function(data) {
      console.log("berjaya login :", data);
           $ionicPopup.alert({
           title: 'Success',
           template: 'Register Success'
         })

        .then(function(res) {
    $location.path('/loginTab/login');
   });

    })
    .error(function(data) {
           $ionicPopup.alert({
           title: 'Error',
           template: 'Fail to register'
         });
    })
  }

})

.controller('LoginCtrl', function($scope, $state, $http, $ionicPopup, $location) {

    $scope.loginData = {};

  $scope.doLogin = function()
  {

    var username = $scope.loginData.username;
    var password = $scope.loginData.password;

    console.log(username + password);

        //console.log("time : "+ ntime);

    $http.get("https://syafiqevo.000webhostapp.com/flex/login.php?&username="+username+"&password="+password).
    success(function(data) {
      console.log("berjaya login :", data[0]);
      if (data[0] == undefined)
      {
          $ionicPopup.alert({
           title: 'Error',
           template: 'Incorrect username or password'
         });
      }
      else
      {
          var userId = data[0].userId;
          var username = data[0].username;
          var faculty = data[0].faculty;
          var matric = data[0].matric;

          window.localStorage.setItem("userId", userId);
           window.localStorage.setItem("username", username);
           window.localStorage.setItem("matric", matric);
          $state.go('app.course');      
      }

    })
    .error(function(data) {
           $ionicPopup.alert({
           title: 'Error',
           template: 'Fail to login'
         });
    })
  }




  $scope.doRegister = function()
  {
    $location.path('/registerTab/register');
  }
})


.controller('CourseCtrl', function($scope, $state) {
  $scope.cour = "course";

  $scope.goFaculty = function(id)
{
  if (id == 1)
  {
      $state.go('app.ftmk'); 
  }
  else if (id == 2)
  {
      $state.go('app.fkekk');     
  }
  else if (id == 3)
  {
      $state.go('app.ftk');     
  }
  else if (id == 4)
  {
      $state.go('app.fkm');     
  }
  else if (id == 5)
  {
      $state.go('app.fke');     
  }
  else if (id == 6)
  {
      $state.go('app.fkp');     
  }
  else if (id == 7)
  {
      $state.go('app.fptt');     
  }
}


})
.controller('FTMKCtrl', function($scope, $stateParams,$ionicPopup, $http) {
  $scope.ftmk = "ftmk";

var clue = "communication";
    $http.get("https://syafiqevo.000webhostapp.com/flex/getFaculty.php?&clue="+clue).
    success(function(data) {

      $scope.data = data;

      console.log($scope.data);

    })

$scope.showConfirm = function(id) {
      $http.get("https://syafiqevo.000webhostapp.com/flex/getFacultyId.php?&tutorId="+id).
    success(function(data) {

      $scope.subject = data[0].subject;
      $scope.date = data[0].date;
      $scope.time = data[0].time;
      $scope.venue = data[0].venue;
      $scope.description = data[0].description;

      console.log("subjectt", data[0].subject);

   var confirmPopup = $ionicPopup.confirm({
     title: 'Confirm Subject',
     template: 'Subject Name: '+$scope.subject+' <br /> Date: '+$scope.date+'<br /> Time: '+$scope.time+' <br/> Venue: '+$scope.venue+'<br/> Description:'+$scope.description
   });

   confirmPopup.then(function(res) {
     if(res) {
       console.log('You are sure');
       var userId = localStorage.getItem("userId");
           $http.get("https://syafiqevo.000webhostapp.com/flex/confirm.php?&tutorId="+id+"&userId="+userId).
    success(function(data) {

      console.log($scope.data);

    })
     } else {
       console.log('You are not sure');
     }
   });

    })


 };

})


.controller('FKEKKCtrl', function($scope, $stateParams) {
  $scope.fkekk = "fkekk";
})

.controller('FTKCtrl', function($scope, $stateParams) {
  $scope.ftk = "ftk";
})

.controller('FKMCtrl', function($scope, $stateParams) {
  $scope.fkm = "fkm";
})

.controller('FKECtrl', function($scope, $stateParams) {
  $scope.fke = "fke";
})

.controller('FKPCtrl', function($scope, $stateParams) {
  $scope.fkp = "fkp";
})

.controller('FPTTCtrl', function($scope, $stateParams) {
  $scope.fptt = "fptt";
})

.controller('ReminderCtrl', function($scope, $http,$ionicPopup) {
  $scope.r = "Reminder";

  var clue = "a";
    $http.get("https://syafiqevo.000webhostapp.com/flex/getFaculty.php?&clue="+clue).
    success(function(data) {

      $scope.data = data;

      console.log($scope.data);

$scope.showAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'Subject',
       template: 'Time: <br /> Date: <br /> Venue: <br /> Description:'
       
     });

}
    })




})
.controller('ReminderdetailCtrl', function($scope, $stateParams) {
  $scope.d = "Reminderdetail";
});



